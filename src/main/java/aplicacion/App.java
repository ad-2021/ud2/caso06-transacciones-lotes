package aplicacion;

import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

public class App {
	static Connection con;

	public static void main(String[] args) {

		try {
			con = ConexionBD.getConexion();

//			DatabaseMetaData dm;
//			dm = con.getMetaData();
//			System.out.println("Soporta transacciones -> " + dm.supportsTransactions());
//			System.out.println("Estado autocommit actual: " + con.getAutoCommit());
//			System.out.println("Nivel de aislamiento actual: " + con.getTransactionIsolation() + " -> "
//					+ Connection.TRANSACTION_REPEATABLE_READ);
//			System.out.println("Soporta procesamiento batch -> " + dm.supportsBatchUpdates());
		} catch (SQLException e) {
			System.err.println("Error de conexión: " + e.getMessage());
		}

		if (con != null) {
			realizaTransaccion();
//			realizaBatch();
		}

		try {
			ConexionBD.cerrar();
		} catch (SQLException e) {
			System.err.println("Error cerrando conexión: " + e.getMessage());
		}

	}

	private static void realizaTransaccion() {
	        try (Statement st1 = con.createStatement(); Statement st2 = con.createStatement();
	        		Statement st3 = con.createStatement();) {
	        	
	            con.setAutoCommit(false);
	            st1.executeUpdate("UPDATE empresa_ad.lineas_factura SET cantidad=cantidad+5 where linea=1 and factura=1");
	            System.out.println("Actualización1");	       
	            // Podemos provocar error: 
	            // if (...) throw new SQLException();
	            // ó también:
	            st2.executeUpdate("UPDATE empresa_ad.lineas_factura SET factura = 8000 where linea=1 and factura=1");
//	            st2.executeUpdate("UPDATE empresa_ad.lineas_factura SET cantidad=cantidad-10 where linea=1 and factura=1");
	            System.out.println("Actualización2");
	            st3.executeUpdate("UPDATE empresa_ad.lineas_factura SET cantidad=cantidad+1 where linea=1 and factura=1");
	            System.out.println("Actualización3");

	            con.commit();
	            System.out.println("Fin transacción: todo ok");
	            
	        } catch (SQLException e) {
	            System.err.println("Error en transacción... " + e.getMessage());
	            try {
	                System.out.println("Haciendo rollback...");
	                con.rollback();
	            } catch (SQLException ex) {
	                System.err.println("Error en rollback... " + ex.getMessage());
	            }
	        } finally {
	            try {	                
	                System.out.println("Ponemos autocommit a true (automáticamente hace commit)");
	                con.setAutoCommit(true);
	            } catch (SQLException ex) {
	                System.err.println("Error activando autocommit true..." + ex.getMessage());
	            }
	        }

	}

	private static void realizaBatch() {
		int[] result = null;
        try (Statement st = con.createStatement()) {
            con.setAutoCommit(false);
            st.addBatch("insert into empresa_ad.articulos(nombre, precio, codigo, grupo) values('HD 120G',120.0,'HD120',1)");
         // Podemos provocar error: poner grupo que no exista o una tabla que no existe.
            st.addBatch("insert into empresa_ad.articulos(nombre, precio, codigo, grupo) values ('HD 160G',160.0,'HD160',2)");            
            st.addBatch("update empresa_ad.articulos set precio = precio - 2 where precio > 1200");
                     
            result = st.executeBatch();
            
            System.out.println("TODO BIEN! Resultado: " + Arrays.toString(result));
            // ConexionBD.cerrar(); // provocando error fin conexion cerrada. Si esto ocurre: no hará nada.
            con.commit();
        } catch (BatchUpdateException ex) {
            System.err.println("Error Batch");
            int[] r = ex.getUpdateCounts();
            System.out.println(Arrays.toString(r));
            try {
            	// ConexionBD.cerrar(); // provocando error fin conexion cerrada. Si esto ocurre: no hará nada.
                con.commit();
//                con.rollback();
            } catch (SQLException ex1) {
                System.out.println("error haciendo rollback1");
            }

        } catch (SQLException ex) {
            System.err.println("Error sql");            
            try {
                // con.commit();
                con.rollback();
            } catch (SQLException ex1) {
                System.out.println("error haciendo rollback2");
            }
        } finally {
            System.out.println("Fin lote");
            try {
                // Hace commit automaticamente
                con.setAutoCommit(true);
            } catch (SQLException ex) {
                System.out.println("error poniendo autocommit a true");
            }
        }

	}

}
